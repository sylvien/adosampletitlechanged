﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
INSERT INTO Category (Name) VALUES
	('ELECTRO'),
	('RAP'),
	('ROCK'),
	('JAZZ'),
	('SONATE');

	--INSERT INTO Song (Title, Duration, CategoryId, ArtistId) VALUES
	--('Tiggre', 250, 1, 1),
	--('Mirroir', 315, 1, 2);

	--INSERT INTO Artist (Pseudo, LastName, FirstName, BirthDate) VALUES
	--('LE COLISEE', 'NZEYIMANA', 'David', 2000-01-01),
	--('PARADIS', '', '', 2000-01-01),
	--('KING OF THE POP', 'JACKSON', 'Michael', 2000-01-01),
	--('KING OF FUNK', 'NELSON', 'Prince', 2000-01-01);