﻿using ADOSample.DAL.Entities;
using ADOSample.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ADOSample.Console
{
    class Program
    {
        static ArtistRepository repArt = new ArtistRepository();
        static void Main(string[] args)
        {
            #region CATEGORIES

            //CategoryRepository repoCategory = new CategoryRepository();         // ! using REPOSITORIES alt+enter >> récupérer repositories

            #region GetAll() - afficher la liste de toutes les CATEGORIES

            //List<Category> categories = repoCategory.GetAll();                  // ! using ENTITIES alt+enter
            // liste récupérant le retour de la fonction GetAll

            //IEnumerable<Category> categories = repoCategory.GetAll();

            //foreach (var c in categories)                                       // pour chaque catégorie contenue dans categories
            //{
            //    System.Console.WriteLine($"{c.Id}\t {c.Name}");                 // on affiche ses id et nom
            //}
            #endregion

            #region Get(int) - afficher le NOM de la CATEGORIE correspondant au numéro entré

            //System.Console.WriteLine("Quel est le numéro de la catégorie dont vous voulez connaitre le nom ?");
            //int choix =  Convert.ToInt32(System.Console.ReadLine());
            //Category cat = repoCategory.Get(choix);

            //System.Console.WriteLine(cat!= null ? cat.Name : "Not existing");
            #endregion

            #region Insert(Category) - ajouter une nouvelle catégorie

            //System.Console.WriteLine("Entrez une nouvelle catégorie :");
            //string name = System.Console.ReadLine();

            //Category nouvelle = new Category();
            //nouvelle.Name = name;

            //int categorieId = repoCategory.Insert(nouvelle);
            //System.Console.WriteLine(categorieId);
            #endregion

            #region Delete(int) - supprimer une catégorie

            //System.Console.WriteLine("Quel est l'ID de la catégorie à supprimer ?");
            //int poubelle = Convert.ToInt32(System.Console.ReadLine());
            //System.Console.WriteLine(repoCategory.Delete(poubelle)?$"La categorie corespondant à l'ID {poubelle} a bien été supprimé" : $"Aucune suppression : aucune catégorie correspondant à l'ID {poubelle}");
            #endregion

            #region Update(category) - modifier une catégorie
            //Category aChanger = new Category();
            //System.Console.WriteLine("Quel est le nom OU l'id de la catégorie à modifier ?");
            //string STRchangement = System.Console.ReadLine();
            //int INTchangement;

            //if (Int32.TryParse(STRchangement, out INTchangement))
            //{
            //    aChanger.Id = INTchangement;
            //}
            //else
            //{
            //    aChanger.Name = STRchangement;
            //}

            //bool reussi = repoCategory.Update(aChanger);

            //System.Console.WriteLine(reussi ? "changement effectué" : "too bad");

            //Int32.TryParse(STRchangement, out INTchangement) ? (aChanger.Id = INTchangement) : (aChanger.Name = STRchangement);
            #endregion

            #endregion

            #region CATEGORIES 2

            //bool continuer = true;

            //while (continuer)
            //{
            //    System.Console.WriteLine("\n1. Afficher toutes les catégories");
            //    System.Console.WriteLine("2. Créer une nouvelle catégorie");
            //    System.Console.WriteLine("3. Supprimer une catégorie");
            //    System.Console.WriteLine("4. Modifier une catégorie");
            //    System.Console.WriteLine("5. Effacer l'écran");
            //    System.Console.WriteLine("6. Arreter !\n");

            //    int choixBis;
            //    do
            //    {
            //        System.Console.WriteLine("Faites un choix :");
            //    } while (!int.TryParse(System.Console.ReadLine(), out choixBis) || choixBis > 6);


            //    CategoryRepository repCat = new CategoryRepository();

            //    switch (choixBis)
            //    {
            //        case 1:
            //            IEnumerable<Category> cats = repCat.GetAll();
            //            foreach (Category item in cats)
            //            {
            //                System.Console.WriteLine($"{ item.Id}. { item.Name}");
            //            }
            //            break;
            //        case 2:
            //            System.Console.WriteLine("Entrez le nom de la categorie à ajouter");
            //            string entree = System.Console.ReadLine();
            //            //Category c = new Category({Name = entree });
            //            Category c = new Category();
            //            c.Name = entree;
            //            int cId = repCat.Insert(c);
            //            System.Console.WriteLine($"L'id de l'insertion est : {cId}");
            //            break;
            //        case 3:
            //            System.Console.WriteLine("Quel est l'id de la catégorie à supprimer?");
            //            int aSup;
            //            do
            //            {

            //            } while (!int.TryParse(System.Console.ReadLine(), out aSup));

            //            System.Console.WriteLine(repCat.Delete(aSup) ? $"La categorie corespondant à l'ID {aSup} a bien été supprimée" : $"Aucune suppression : aucune catégorie correspondant à l'ID {aSup}");
            //            break;
            //        case 4:
            //            System.Console.WriteLine("Quel est le nom ou id de la catégorie à modifier");
            //            Category toModify = new Category();

            //            string STRParam = System.Console.ReadLine();
            //            int INTParam;

            //            if (Int32.TryParse(STRParam, out INTParam))
            //            {
            //                toModify.Id = INTParam;
            //            }
            //            else
            //            {
            //                toModify.Name = STRParam;
            //            }
            //            bool updated = repCat.Update(toModify);

            //            System.Console.WriteLine(updated ? "changement effectué" : "too bad");
            //            break;
            //        case 5:
            //            System.Console.Clear();
            //            break;
            //        case 6:
            //            continuer = false;
            //            break;
            //        default:
            //            break;
            //    }
            //}
            #endregion

            #region ARTIST

            //bool continueArtist = true;

            //do
            //{
            //    SeeMenu();

            //    int choix;
            //    do
            //    {
            //        System.Console.WriteLine("\nFaites un choix");
            //    } while (!int.TryParse(System.Console.ReadLine(), out choix) || choix > 5);

            //    switch (choix)
            //    {
            //        case 1:
            //            SeeArtists();
            //            break;
            //        case 2:
            //            AddArtist();
            //            SeeArtists();
            //            break;
            //        case 3:
            //            SeeArtists();
            //            UpdateArtist();
            //            SeeArtists();
            //            break;
            //        case 4:
            //            SeeArtists();
            //            DeleteArtiste();
            //            SeeArtists();
            //            break;
            //        case 5:
            //            continueArtist = false;
            //            break;
            //        default:
            //            break;
            //    }
            //    System.Console.ReadKey();
            //} while (continueArtist);
            #endregion
        }


    //    private static void SeeMenu()
    //    {
    //        System.Console.Clear();
    //        System.Console.WriteLine("1. Voir tous les artistes");
    //        System.Console.WriteLine("2. Ajouter un artiste");
    //        System.Console.WriteLine("3. Modifier un artiste");
    //        System.Console.WriteLine("4. Supprimer un artiste");
    //        System.Console.WriteLine("5. Arreter");
    //    }

    //    private static void SeeArtists()
    //    {
    //        System.Console.Clear();
    //        IEnumerable<Artist> mesArtistes = repArt.GetAll();

    //        foreach (var item in mesArtistes)
    //        {
    //            string pseudo = item.Pseudo != null ? item.Pseudo : " / \t";
    //            string firstname = item.FirstName != null ? item.FirstName : " / \t";
    //            string lastname = item.LastName != null ? item.LastName : " / \t";
    //            string birthdate = item.BirthDate != null ? item.BirthDate.ToString() : " / \n";
    //            System.Console.WriteLine($"{item.Id} : \t{pseudo} >> {firstname} >> {lastname} >> {birthdate}");        // !! PROBLEME id
    //        }
    //    }

    //    private static void AddArtist()
    //    {
    //        System.Console.Clear();
    //        Artist newOne = new Artist();
    //        System.Console.Write("Quel est son PSEUDO (enter si pas) :");
    //        string pseudo = System.Console.ReadLine();

    //        System.Console.Write("Quel est son NOM (enter si pas) :");
    //        string nom = System.Console.ReadLine();

    //        System.Console.Write("Quel et son PRENOM (enter si pas) :");
    //        string prenom = System.Console.ReadLine();

    //        System.Console.Write("Quele son NASISSANCE (YYYY-MM-DD ou enter si pas) :");
    //        string naissanceSTR = System.Console.ReadLine();
    //        DateTime? naissanceDT;
    //        if (naissanceSTR != "")
    //        {
    //            naissanceDT = DateTime.Parse(naissanceSTR);
    //        }
    //        else
    //        {
    //            naissanceDT = null;
    //        }

    //        newOne.Pseudo = pseudo;
    //        newOne.LastName = nom;
    //        newOne.FirstName = prenom;
    //        newOne.BirthDate = naissanceDT;

    //        int artistId = repArt.Insert(newOne);
    //        System.Console.WriteLine($"L'id de cet artiste est {artistId}");
    //        System.Console.ReadKey();
    //    }

    //    private static void UpdateArtist()
    //    {
    //        System.Console.WriteLine("Quel est l'ID de l'artiste à modifier ?");

    //        int idToModify = int.Parse(System.Console.ReadLine());
    //        Artist toUpdate = repArt.Get(idToModify);

    //        if (toUpdate != null)
    //        {

    //            System.Console.WriteLine("Nouveau PSEUDO (ou enter) : ");
    //            toUpdate.Pseudo = System.Console.ReadLine();

    //            System.Console.WriteLine("Nouveau NOM (ou enter) : ");
    //            toUpdate.LastName = System.Console.ReadLine();

    //            System.Console.WriteLine("Nouveau PRENOM (ou enter) : ");
    //            toUpdate.FirstName = System.Console.ReadLine();

    //            System.Console.WriteLine("Nouveau NAISSANCE yyyy-mm-dd (ou enter) : ");
    //            string newDate = System.Console.ReadLine();
    //            if (newDate != "")
    //            {
    //                toUpdate.BirthDate = DateTime.Parse(newDate);
    //            }

    //            System.Console.WriteLine(repArt.Update(toUpdate) ? "modifications effectuées" : "une erreur s'est produite");
    //            System.Console.ReadKey();
    //        }
    //        else
    //        {
    //            System.Console.WriteLine($"Aucun artist avec l'id {idToModify}");
    //            System.Console.ReadKey();
    //        }
    //    }

    //    private static void DeleteArtiste()
    //    {

    //        int choix;
    //        do
    //        {
    //            System.Console.WriteLine("Quel est l'id de l'artiste à supprimer ?");
    //        } while (!int.TryParse(System.Console.ReadLine(), out choix) || repArt.Get(choix) == null);

    //        System.Console.WriteLine(repArt.Delete(choix) ? "suppression effectuée" : "une erreur s'est produite");
    //        System.Console.ReadKey();
    //    }
    }
}
