﻿using ADOSample.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOSample.DAL.Repositories
{
    class SongRepository : IRepository<Song>
    {
        string _ConnectionString = @"Data Source=WAD-6\ADMINSQL;Initial Catalog=ADOSample;Integrated Security=True";
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Song Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Song> GetAll()
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();

                string query = "SELECT * FROM Song";
                SqlCommand cmd = new SqlCommand(query, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                List<Song> result = new List<Song>();

                while (reader.Read())
                {
                    Song s = new Song();
                    s.Id = (int)reader["Id"];
                    s.Title = reader["Title"] as string;
                    s.Duration = (int)reader["Duration"];
                    s.CategoryId = (int)reader["CategoryId"];
                    s.ArtistId = (int)reader["ArtistId"];
                    result.Add(s);
                }

                return result;
            }
        }

        public int Insert(Song item)
        {
            throw new NotImplementedException();
        }

        public bool Update(Song item)
        {
            throw new NotImplementedException();
        }
    }
}
