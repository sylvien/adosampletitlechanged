﻿using ADOSample.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOSample.DAL.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {            
        private string _ConnectionString
            = @"Data Source=WAD-6\ADMINSQL;Initial Catalog=ADOSample;Integrated Security=True"; // création d'une variable qui contiendra la connection

        public bool Delete(int id)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();

                if (this.Get(id) != null)
                {
                    string query = "DELETE FROM Category WHERE Id = @id";

                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@id", id);

                    cmd.ExecuteNonQuery();

                    return true;
                }

                return false;

                //conn.Close();
            }
        }

        public Category Get(int id)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();                                                                    
                string query = "SELECT * FROM CATEGORY WHERE ID = @id";                         // string query = $"SELECT * FROM CATEGORY WHERE ID = {id}";

                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@id", id);                                         // pour vérifier et éviter injection sql

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    Category category = new Category();
                    category.Id = (int)reader["Id"];
                    category.Name = reader["Name"] as string;
                    return category;
                }

                conn.Close();

                return null;
            }
        }

        /// <summary>
        /// Get All the records from Category
        /// </summary>
        ///<exception cref="Exception"></exception>
        /// <returns>A list of instances of Categories</returns>
        public IEnumerable<Category> GetAll()
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))                   // hérite de DbConnection & implemente IDisposable
            {
                conn.Open();                                                                    // OUVRIR connection
                string query = "SELECT * FROM CATEGORY";                                        // requete sql en string

                SqlCommand cmd = conn.CreateCommand();                                          // creation de COMMANDE
                cmd.CommandText = query;                                                        // à laquelle on ajoute la requete

                SqlDataReader reader = cmd.ExecuteReader();                                     // LIRE DONNEES

                List<Category> result = new List<Category>();                                   // créer liste de categories vide

                //while (reader.Read())                                                         // AVEC IENUMERABLE
                //{
                //    Category c = new Category();
                //    c.Id = (int)reader["Id"];
                //    c.Name = reader["Name"] as string;                                          
                //    yield return c;                                                             
                //}

                while (reader.Read())                                                           // retourne un bool / va à la ligne suivate
                {
                    Category c = new Category();
                    c.Id = (int)reader["Id"];
                    c.Name = reader["Name"] as string;                                          // AS que si la valeur par defaut est null pas zéro
                    result.Add(c);                                                              // ajouter à la liste
                }

                return result;

                //conn.Close();                                                                 // FERMER connection, facultatf car la méthode "dispose" le fait
            }
            //  A la fin du using la méthode dispose de ma connection libère les ressources non managées par le garbage collector.
        }

        public int Insert(Category item)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))                       // utiliser connection
            {
                conn.Open();                                                                        // ouverture connection

                string query = "INSERT INTO Category (Name) OUTPUT inserted.Id VALUES (@name)";     // requete avec parametre dont faudra spécifier parma
                
                SqlCommand cmd = new SqlCommand(query, conn);                                       // créer commande et lui associer requete
                // revient au même que ci-dessous:
                //SqlCommand cmd = conn.CreateCommand();
                //cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@name", item.Name);                                    // lier à parametre de la requete

                //int o = (int)cmd.ExecuteScalar();
                //conn.Close();
                //return o;

                return (int)cmd.ExecuteScalar();
            }
        }

        public bool Update(Category item)                                                       // similaire à delete, git le projet de Khun
        {
            Console.WriteLine("Quel sera le nouveau nom ?");                                        // !!! normalement pas de console dans le DAL
            string nouveauNom = Console.ReadLine();

            if (this.Get(item.Id) != null)
            {
                using (SqlConnection conn = new SqlConnection(_ConnectionString))
                {
                    conn.Open();

                    //string query = "UPDATE Category SET Name = @name WHERE Id = @id";

                    SqlCommand cmd = conn.CreateCommand();
                    //cmd.CommandText = query;
                    cmd.CommandText = "UPDATE Category SET Name = @name WHERE Id = @id";            // peut être mis directement sans passer par une variable string
                    cmd.Parameters.AddWithValue("@name", nouveauNom);
                    cmd.Parameters.AddWithValue("@id", item.Id);

                    return cmd.ExecuteNonQuery() != 0;
                }
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(_ConnectionString))
                {
                    conn.Open();

                    string query = "UPDATE Category SET Name = @new WHERE Name = @name ";

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@new", nouveauNom);
                    cmd.Parameters.AddWithValue("@name", item.Name);

                    //if (cmd.ExecuteNonQuery() != 0)
                    //{
                    //    return true;
                    //}
                    //return false;

                    return cmd.ExecuteNonQuery() != 0;
                }
            }
        }
    }
}
