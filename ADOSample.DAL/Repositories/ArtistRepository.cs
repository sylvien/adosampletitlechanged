﻿using ADOSample.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOSample.DAL.Repositories
{
    public class ArtistRepository : IRepository<Artist>
    {
        private string _ConnectionString = @"Data Source=WAD-6\ADMINSQL;Initial Catalog=ADOSample;Integrated Security=True";
        public bool Delete(int id)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();
                string query = "DELETE FROM Artist WHERE Id = @id";

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("id", id);

                return cmd.ExecuteNonQuery() != 0;
            }
        }

        public Artist Get(int id)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();
                string query = "SELECT * FROM Artist WHERE Id=@id";

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("id", id);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Artist selectedOne = new Artist();
                    selectedOne.Id = (int)reader["Id"];
                    selectedOne.Pseudo = reader["Pseudo"] as string;
                    selectedOne.LastName = reader["LastName"] as string;
                    selectedOne.FirstName = reader["FirstName"] as string;
                    selectedOne.BirthDate = (DateTime)reader["BirthDate"];
                    return selectedOne;
                }

                return null;

            }
        }

        public IEnumerable<Artist> GetAll()
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM Artist";

                SqlDataReader reader = cmd.ExecuteReader();

                List<Artist> result = new List<Artist>();

                while (reader.Read())
                {
                    Artist a = new Artist();
                    a.Id = (int)reader["Id"];
                    a.Pseudo = reader["Pseudo"] as string;
                    a.LastName = reader["LastName"] as string;
                    a.FirstName = reader["FirstName"] as string;
                    a.BirthDate = (DateTime)reader["BirthDate"];
                    result.Add(a);
                }
                return result;
            }
        }

        public int Insert(Artist item)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();
                string querry = "INSERT INTO Artist (Pseudo, LastName, FirstName, BirthDate) OUTPUT inserted.Id VALUES (@pseudo, @lastname, @firstname, @birthdate)";

                //SqlCommand cmd = new SqlCommand(querry, conn);
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = querry;
                cmd.Parameters.AddWithValue("pseudo", item.Pseudo);
                cmd.Parameters.AddWithValue("lastname", item.LastName);
                cmd.Parameters.AddWithValue("firstname", item.FirstName);
                cmd.Parameters.AddWithValue("birthdate", item.BirthDate);

                return (int)cmd.ExecuteScalar();
            }
        }

        public bool Update(Artist item)
        {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE Artist SET Pseudo = @pseudo, LastName = @lastname, FirstName = @firstname, BirthDate = @birthdate WHERE Id = @id";

                cmd.Parameters.AddWithValue("@id", item.Id);
                cmd.Parameters.AddWithValue("@pseudo", item.Pseudo);
                cmd.Parameters.AddWithValue("@lastname", item.LastName);
                cmd.Parameters.AddWithValue("@firstname", item.FirstName);
                cmd.Parameters.AddWithValue("@birthdate", item.BirthDate);

                return cmd.ExecuteNonQuery() != 0;
            }
        }
    }
}
