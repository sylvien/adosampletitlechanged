﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOSample.DAL.Repositories
{
    interface IRepository<T>
    {
        T Get(int id);
        //List<T> GetAll();
        IEnumerable<T> GetAll();
        int Insert(T item);
        bool Delete(int id);
        bool Update(T item);
    }
}
