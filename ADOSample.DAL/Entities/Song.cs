﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOSample.DAL.Entities
{
    class Song
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Duration { get; set; }
        public int CategoryId { get; set; }
        public int ArtistId { get; set; }
    }
}
